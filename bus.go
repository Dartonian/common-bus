package bus

import (
	"context"
	"time"

	"github.com/segmentio/kafka-go"
)

// Message struct abstract
type Message struct {
	Topic     string
	Partition int
	Offset    int64
	Key       []byte
	Value     []byte
	Headers   []kafka.Header
	Time      time.Time
}

func (m *Message) convertToKafka() kafka.Message {
	return kafka.Message(*m)
}

// ConvertMessage from kafka message
func ConvertMessage(msg kafka.Message) Message {
	return Message(msg)
}

// Producer struct
type Producer struct {
	*kafka.Writer
}

// CreateProducer returns Producer
func CreateProducer(URL, topic string, delay time.Duration) *Producer {
	return &Producer{createWriter(URL, topic, delay)}
}

func createWriter(URL, topic string, delay time.Duration) *kafka.Writer {
	return &kafka.Writer{
		Addr:         kafka.TCP(URL),
		Topic:        topic,
		Balancer:     &kafka.LeastBytes{},
		RequiredAcks: kafka.RequireAll,
		BatchTimeout: delay * time.Millisecond,
	}
}

// WriteMessages abstraction
func (p *Producer) WriteMessages(ctx context.Context, msgs ...Message) error {

	var kafkaMsgs []kafka.Message

	for _, msg := range msgs {
		kafkaMsgs = append(kafkaMsgs, msg.convertToKafka())
	}

	return p.Writer.WriteMessages(ctx, kafkaMsgs...)
}

// Consumer struct
type Consumer struct {
	*kafka.Reader
}

// CreateConsumer returns Consumer
func CreateConsumer(URL, groupID, topic string) *Consumer {
	return &Consumer{createReader(URL, groupID, topic)}
}

func createReader(URL, groupID, topic string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:     []string{URL},
		GroupID:     groupID,
		StartOffset: kafka.LastOffset,
		Topic:       topic,
		MinBytes:    100,  // 10e3 -- 10KB
		MaxBytes:    10e6, // 10MB
	})
}

// ReadMessage abstraction
func (c *Consumer) ReadMessage(ctx context.Context) (Message, error) {
	msg, err := c.Reader.ReadMessage(ctx)

	return ConvertMessage(msg), err
}
